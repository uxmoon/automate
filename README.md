# Automate your workflow #

Based on the book with same name written by Zell Liew

### Usage ###
* Run `npm install`
* Run `bower install`
* Run `gulp`

### What is this repository for? ###

* One folder structure
* HTML templating using Nunjucks
* Generate sprites images with spritesmith
* Sass Workflow ready, includes Susy, sourcemaps and autoprefixer
* Notify on errors
* BrowserSync for live reload
